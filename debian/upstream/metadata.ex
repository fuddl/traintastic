# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/traintastic/issues
# Bug-Submit: https://github.com/<user>/traintastic/issues/new
# Changelog: https://github.com/<user>/traintastic/blob/master/CHANGES
# Documentation: https://github.com/<user>/traintastic/wiki
# Repository-Browse: https://github.com/<user>/traintastic
# Repository: https://github.com/<user>/traintastic.git
