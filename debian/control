Source: traintastic
Section: misc
Priority: optional
Maintainer: Bruno Kleinert <fuddl@debian.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 cmake,
 libboost-dev,
 libboost-program-options-dev,
 liblua5.3-dev,
 zlib1g-dev,
 libarchive-dev,
 libsystemd-dev,
 nlohmann-json3-dev,
 qt6-base-dev,
 qt6-svg-dev,
 lsb-release,
 pkg-config,
Standards-Version: 4.7.0
Homepage: https://traintastic.org/
#Vcs-Browser: https://salsa.debian.org/debian/traintastic
#Vcs-Git: https://salsa.debian.org/debian/traintastic.git

Package: traintastic
Architecture: any
Multi-Arch: foreign
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 traintastic-common
Description: Client/server software application to control a model railroad
 One PC controls the model railroad layout, multiple other PCs can be used to
 view what is happening.
 .
 Features currently implemented:
  * Control locomotives
  * Control accessory like switches and signals
  * Read feedback sensors
  * Read RailCOM/Lissy sensors
  * Wireless throttles: WLANmaus, Z21 app and WiThrottle.
  * Board to draw a schemetic layout of the model railroad.
 .
 This package installs the visual user interface of Traintastic, i.e., the
 client.

Package: traintastic-server
Architecture: any
Multi-Arch: foreign
Built-Using: ${my:Built-Using}
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 traintastic-common
Description: Client/server software application to control a model railroad
 One PC controls the model railroad layout, multiple other PCs can be used to
 view what is happening.
 .
 Features currently implemented:
  * Control locomotives
  * Control accessory like switches and signals
  * Read feedback sensors
  * Read RailCOM/Lissy sensors
  * Wireless throttles: WLANmaus, Z21 app and WiThrottle.
  * Board to draw a schemetic layout of the model railroad.
 .
 This package installs the software that controls your model railroad. Install
 the traintastic package on the same or another computer in your network to get
 the visual user interface for your model railroad.

Package: traintastic-common
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Suggests: traintastic, traintastic-server
Description: Common and localization files of Traintastic
 This package installs common files of the Traintastic server and client.
 Usually it is pulled in as a dependency and you do not install this package
 manually.

